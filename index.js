const express = require('express');
const app = express();

const campaignCreate = require('./Controller/campaignCreate');
const campaignRead = require('./Controller/campaignRead');
const campaignUpdate = require('./Controller/campaignUpdate');
const campaignDelete = require('./Controller/campaignDelete');



// campaignUpdate.info();


const port = process.env.PORT || 3000;

const server = app.listen(port);

server.timeout = 1000 * 60 * 10; // 10 minutes

// Use middleware to set the default Content-Type
app.use(function (req, res, next) {
    res.header('Content-Type', 'application/json');
    next();
});

app.get('/api/update', (req, res) => {
    res.send(JSON.stringify(campaignUpdate.update()));
})

app.get('/api/create', (req, res) => {
  res.send(JSON.stringify(campaignCreate.create()));
})

app.get('/api/read', (req, res) => {
  res.send(JSON.stringify(campaignRead.read()));
})

app.get('/api/delete', (req, res) => {
res.send(JSON.stringify(campaignDelete.delete()));
})

























// console.log(obj);
//  console.log(campaignUpdate());


// app.get('/', (req, res) => {
    
//   res.send('hello');
// })
// .use('/text', campaignUpdate);

// app.listen(3000);

// const port = process.env.PORT || 3000;

// const server = app.listen(port);

// server.timeout = 1000 * 60 * 10; // 10 minutes

// // Use middleware to set the default Content-Type
// app.use(function (req, res, next) {
//     res.header('Content-Type', 'application/json');
//     next();
// });

// app.get('/api/endpoint1', (req, res) => {
//     res.json(campaignUpdate);
// })

// app.get('/api/endpoint2', (req, res) => {
//     // Set Content-Type differently for this particular API
//     res.set({'Content-Type': 'application/xml'});
//     res.send(`<note>
//         <to>Tove</to>
//         <from>Jani</from>
//         <heading>Reminder</heading>
//         <body>Don't forget me this weekend!</body>
//         </note>`);
// })
// const express = require('express')
// const app = express()

// const getTextMessage =require('./getTextMessage');
// const getMessageJsonFormat =require('./getMessageJsonFormat');


// app.get('/', (req, res) => {  
//   res.send('hello');
// })
// .use('/text', getTextMessage).use('/json', getMessageJsonFormat);

// app.listen(3000);


