const adsSdk = require('facebook-nodejs-business-sdk');
const credentials = require("./credentials");
const accessToken = credentials.api_key; //Get access token from credendtials.js file
const api = adsSdk.FacebookAdsApi.init(accessToken);
const AdAccount = adsSdk.AdAccount;
const Campaign = adsSdk.Campaign;
const account = new AdAccount(credentials.account_id);  // Get account id from credendtials.js file
var campaignRead = {
  read: function (info) {
    account.getCampaigns([Campaign.Fields.name], { limit: 4 })  //  As per requirement you need to change the value of limits
      .then((campaigns) => {
        let results = [];
        if (campaigns.length >= 4 || campaigns.hasNext()) {
          // console.log(campaigns)
          campaigns.forEach((campaign) => results.push({
            "campaign_id": campaign.id,
            "campaign_name": campaign.name
          }))
          console.log(results);
        }
        else {
          Promise.reject(
            new Error(`campaigns length < ${campaigns.length + 1} or not enough campaigns`)
          );
        }
      })
      .catch((error) => {
      });
  }
};
module.exports = campaignRead;



