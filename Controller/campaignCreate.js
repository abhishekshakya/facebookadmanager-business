
const adsSdk = require('facebook-nodejs-business-sdk');
const credentials = require("./credentials");
const accessToken = credentials.api_key; //Get access token from credendtials.js file
const api = adsSdk.FacebookAdsApi.init(accessToken);
const AdAccount = adsSdk.AdAccount;
const Campaign = adsSdk.Campaign;
const account = new AdAccount(credentials.account_id);  // Get account id from credendtials.js file
var campaignCreate = {
  create: function (info) {
    account.createCampaign(
      [],
      {
        [Campaign.Fields.special_ad_category]: 'NONE',
        [Campaign.Fields.name]: 'It services ',  //Change the campaign name according to your requirement
        [Campaign.Fields.status]: Campaign.Status.paused,
        [Campaign.Fields.objective]: Campaign.Objective.page_likes
      }
    )
      .then((campaign) => {
        console.log(campaign);
      })
      .catch((error) => {
        console.log(error);
      });
  }
};
module.exports = campaignCreate;

