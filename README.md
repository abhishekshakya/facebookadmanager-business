# facebookAdManager-Business

this application can fetch ad manager detail from facebook graph api and shows it on the console
you can do CRUD operations using this project

**NOTE:**
if you want to use this application you need to create an app in facebook and get the token key
and account id as well and replace the value accordingly in credentials.js file and you can run it 

once the app is running 
you can navigate
    *  localhost:3000/api/create -> creates the new ad campaign 
    *  localhost:3000/api/update -> updates the ad which is created
    *  localhost:3000/api/read -> shows all created campaigns
    *  localhost:3000/api/delete -> delete the ad by campaign id

    
please see the Contoller files 