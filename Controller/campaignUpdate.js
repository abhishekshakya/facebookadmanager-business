const adsSdk = require('facebook-nodejs-business-sdk');
const credentials = require("./credentials");
const accessToken = credentials.api_key; //Get access token from credendtials.js file
const api = adsSdk.FacebookAdsApi.init(accessToken);
const Campaign = adsSdk.Campaign;
const campaignId = '23844358012560657'; //campaign id 

var updateCampaign = {
  update : function (info) {
    const aa = new Campaign(campaignId, {
      [Campaign.Fields.id]: '23844358012560657', //you need to change campaign id 
      [Campaign.Fields.name]: 'Campaign - Updated'
    })
      .update().then((update) => {
        console.log('Update Successfull');
        console.log(update);
      })
      .catch((error) => {
        console.log(error);
      });
  }
};
module.exports = updateCampaign;
